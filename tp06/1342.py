ativo = True

while ativo == True:
	P, S = input().split()
	P = int(P)
	S = int(S)
	if P == 0 and S == 0:
		ativo = False
	else:
		T1, T2, T3 = input().split()
		T1 = int(T1)
		T2 = int(T2)
		T3 = int(T3)
		N = int(input())
		S += 1

		playerState = (P+1)*[0]
		playerPosition = (P+1)*[0]
		turn = 0
		rollCount = 0
		winner = 11
		while rollCount < N:
			playerTurn = True
			if playerState[turn] == 1:
			    playerState[turn] = 0
			    turn += 1
			    playerTurn = False
			elif playerState[turn] == 0 and playerTurn == True:
				D1, D2 = input().split()
				result = int(D1) + int(D2) + playerPosition[turn]
				playerPosition[turn] = result
				for i in range(3):
					if result == T1 or result == T2 or result == T3:
						playerState[turn] = 1
			if playerPosition[turn] >= S and winner == 11:
				winner = turn
			if playerTurn == True:
				rollCount += 1
				turn += 1
			if turn == P:
				turn = 0
		winner = winner + 1
		print(winner)
