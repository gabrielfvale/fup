e = 1
tons = 5 * [0]
resps = ['A', 'B', 'C', 'D', 'E', '*']
resultado = ''
primeiro = True
# Pede os casos de teste
while e > 0:
	e = int(input())
	i = 0
	if e > 0:
		# Pede as respostas entradas
		for k in range(e):
			total = 0
			marcada = 0
			tons = input().split()
			for c in range(5):
				tons[c] = int(tons[c])
				if tons[c] <= 127:
					marcada = c
					total += 1
			if primeiro:
				if total != 1:
					resultado += resps[5]
				else:
					resultado += resps[marcada]
				primeiro = False
			else:
				if total != 1:
					resultado += '\n' + resps[5]
				else:
					resultado += '\n' + resps[marcada]
print(resultado)