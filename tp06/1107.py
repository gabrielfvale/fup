active = True
while active:
	A, C = input().split()
	A = int(A)
	C = int(C)
	if A == 0 and C == 0:
		active = False
	else:
		activations = 0
		new_shape = input().split()
		currentLevel = A
		for i in range(C):
			if int(new_shape[i]) < currentLevel:
				activations += currentLevel - int(new_shape[i])
			currentLevel = int(new_shape[i])
		print(activations)


