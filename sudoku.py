from os import system
system('clear')

# Util
CORES = {
	'PB': '\033[0;30;47m',
	'VERM': '\033[0;31;47m',
	'END': '\033[0m'
}
SEPARADOR1 = '  ++---+---+---++---+---+---++---+---+---++  \n'
SEPARADOR2 = '  ++===+===+===++===+===+===++===+===+===++  \n'
LETRAS = '     A   B   C    D   E   F    G   H   I     \n'
TABULEIRO = CORES['PB'] + '\n' + LETRAS + SEPARADOR1
MATRIZ = [[' ' for i in range(9)] for i in range(9)]
COLUNAS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']


# Abre arquivo de configuracao e constroi a matriz
qnt = 0
with open('settings.txt', 'r') as settings:
	for l in settings:
		l = l.strip()
		row = int(l[2:3])-1
		col = COLUNAS.index(l[:1].lower())
		num = int(l[-1])
		MATRIZ[row][col] = num
		qnt += 1


# Constroi o TABULEIRO
for i in range(9):
	linha = ' {}|'.format(i+1)
	for j in range(9):
		if j % 3 == 0:
			#linha += '| {} |'.format(MATRIZ[i][j])
			linha += '| {}{}{} |'.format(CORES['VERM'], MATRIZ[i][j], CORES['PB'])
		else:
			#linha += ' {} |'.format(MATRIZ[i][j])
			linha += '{} {} {}|'.format(CORES['VERM'], MATRIZ[i][j], CORES['PB'])
	linha += '|{} \n'.format(i+1)
	TABULEIRO += linha
	if i == 2 or i == 5:
		TABULEIRO += SEPARADOR2
	else:
		TABULEIRO += SEPARADOR1
TABULEIRO += LETRAS + CORES['END']
print(TABULEIRO)
