ativo = 1
while ativo:
	N, B = input().split()
	N = int(N)
	B = int(B)
	if N == 0 and B == 0:
		ativo = 0
	else:

		possivel = 1
		lista = input().split()

		for i in range(B):
			lista[i] = int(lista[i])

		resultados = (N+1)*[0]

		for k in range(B):
			for j in range(k+1):
				diff = abs(lista[k]-lista[j])
				resultados[diff] = 1

		for num in resultados:
			if num != 1:
				possivel = 0

		if possivel:
			print('Y')
		else:
			print('N')