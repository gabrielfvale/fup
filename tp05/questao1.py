'''
Faca um programa em Python que receba e armazene 10 valores numericos. Em seguida, calcule e
mostre o somatorio e o produtorio desses numeros.
'''
i = 0
soma = 0
produto = 1
N = 10 * [0]
while i < 10:
	num = int(input('Digite um numero: '))
	N[i] = num
	i += 1
for num in N:
	soma += num
	produto *= num
print('Soma: {} \nProduto: {}'.format(soma, produto))
