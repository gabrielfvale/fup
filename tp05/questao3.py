'''
Faca um programa em Python que receba N (com 6 ≤ N ≤ 10) numeros de uma cartela da Mega
Sena jogada por um usuario. Em seguida, leia os seis numeros sorteados na Mega Sena. O algoritmo
devera mostrar a quantidade de acertos do apostador com sua cartela, os numeros que ele acertou
e se ele fez a Mega-Sena (6 acertos), a Quina (5 acertos) ou a Quadra (4 acertos).
'''
i = 0
numeros = 6 * [0]
cartela = 10 * [0]
acertos = 0
entrada = 0
print("Digite o numero escrito na cartela, escrevendo um negativo para parar: ")
while i < 10 and entrada >= 0:
	entrada = int(input())
	inList = False
	if entrada < 0 and i < 6:
		print('Entre pelo menos 6 numeros.')
		entrada = 0
	else:
		for k in range(0, i):
			if entrada == cartela[k]:
				inList = True
		if inList:
			print('Numero ja adicionado.')
		if entrada >= 0 and not inList:
			cartela[i] = entrada
			i += 1
print("Digite os numero sorteados: ")
for n in range(0, 6):
	num = int(input())
	numeros[n] = num
acertados = ''
for n in range(0, 10):
	if cartela[n] in numeros:
		acertos += 1
		acertados += ', ' + str(cartela[n])
print('Voce acertou {} numeros, os quais{} estavam em sua cartela.'.format(acertos, acertados))
if acertos == 6:
	print('Voce ganhou na Mega-Sena!')
if acertos == 5:
	print('Voce ganhou na Quina!')
if acertos == 4:
	print('Voce ganhou na Quadra!')
if acertos < 4:
	print('Nao foi dessa vez.')
