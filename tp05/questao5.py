'''
. Faca um programa em Python que receba valores para uma matriz de 20 × 20, substitua e mostre
os valores da diagonal principal como sendo o somatorio dos demais valores de sua linha subtraıdo
pelo somatorio dos demais valores de sua coluna.
'''
s = 20
diagonal = ''
matriz = [[0 for i in range(s)] for i in range(s)]

# Definindo matriz
for row in range(s):
	for col in range(s):
		number=int(input('Digite o valor da posicao {}, {}: '.format(row+1, col+1))) 
		matriz[row][col] = number
# Alterando valores da diagonal
for n in range(s):
	matriz[n][n] = 0
	# Adicionando somatorio dos valores da linha
	for c in range(s):
		if c != n:
			matriz[n][n] += matriz[n][c]
	# Removendo somatorio dos valores da coluna
	for r in range(s):
		if r != n:
			matriz[n][n] -= matriz[r][n]
for i in range(s):
	diagonal += ', ' + str(matriz[i][i])
diagonal = diagonal.lstrip(', ')
print('Os valores da diagonal principal ficaram os seguintes: ' + diagonal)