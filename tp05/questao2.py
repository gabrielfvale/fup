'''
Faca um programa em Python que receba ate 100.000 valores nao negativos. Termine as entradas
com um valor negativo. O algoritmo deve, entao, mostrar todos os numeros distintos e, para cada
numero, quantas vezes ele foi entrado.
'''
ativo = True
i = 0
tamanho = 100000
numeros = tamanho * [0]
quantidades = tamanho * [0]
for q in range(0, tamanho):
	quantidades[q] = 1
while i < tamanho and ativo:
	num = int(input('Digite um numero positivo: '))
	repetido = False
	if num < 0:
		ativo = False
	else:
		for k in range(0, i):
			if num == numeros[k]:
				quantidades[k] += 1
				repetido = True
		if not repetido:
			numeros[i] = num
			i += 1
for m in range(0, i):
	print('O numero {} foi entrado {} vez(es).'.format(numeros[m], quantidades[m]))