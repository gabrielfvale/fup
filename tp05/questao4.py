'''
Faca um programa em Python que receba um determinado valor N e, em seguida, receba N valores
inteiros do usuario. O algoritmo devera determinar se estes numeros, na ordem em que foram entrados,
sao ou nao uma progressao geometrica. Se sim, mostrar a razao desta progressao geometrica.
Considere N no intervalo [0;1000].
'''
razao = 0
PG = True
nums = 1000 * [0]
N = int(input('Digite um valor inteiro positivo: '))
if N >= 2:
	for i in range(0, N):
		entrada = int(input('Digite um valor inteiro: '))
		nums[i] = entrada
		razao = nums[1]/nums[0]
	for i in range(0, N-1):
		if not nums[i+1]/nums[i] == razao:
			PG = False
	if PG:
		print('Os numeros entrados formam uma PG de razao {}.'.format(int(razao)))
	else:
		print('Os numeros entrados nao formam uma PG.')
else:
	print('Nao eh possivel formar uma PG com a quantidade de numeros entrados.')